### DSIP Laboratory work 2 ###

* OAuth2 API Provider
* Entities - tasks and their parameters (metamdel)

### How to start? ###
* Needed Postgres server on 5432 with postgres/postgres credentials
* Simply run jar to deploy (will be on localhost:8080)

### API ###

## Public (wo access token) ##
* GET /api/tasks/types?page=0&size=10
* GET /api/tasks/parameters/types?page=0&size=10

## Via access token ##
Token is transmitted via the Authorization header. You can use predefined user with id=1 and Access Token - c6a95d32-2873-4fb0-bce0-485987d30015, Refresh token - 81a225ab-b3d9-4944-bfdc-7edd9ff37499

* GET    /api/me
* GET    /api/tasks?page=0&size=10
* GET    /api/task/1/children?page=2&size=5
* POST   /api/task?name=Test task&parent=1&taskType=3
* PATCH  /api/task/2?name=Test task modified&taskType=2
* DELETE /api/task/2
* DELETE /api/task/1/children
* GET    /api/task/1/parameters?page=1&size=3
* POST   /api/task/1/parameter?type=4&value=Value
* PATCH  /api/task/2/parameter/1?value=New value
* DELETE /api/task/2/parameter/1

### OAuth2 ###
* External Resource registration is not added :( so you can only play with predefined resouce:
* * id = 1
* * client_id = 'cno947t47t9tc984p9s84'
* * client_secret = 'a0wcn789tp029r123rdghjfznzpc83r48t2twe'
* Authentication:
* * go to /authentication with query params = client_id=cno947t47t9tc984p9s84&redirect_url=http://localhost:1111/forcode&response_type=code
* * on form use a@b.c as email and 1 as password (sign up backend not complited :((( )
* * on Access form click allow and you will be redirected to http://localhost:1111/forcode with code in query params
* * Perform POST request to /oauth2/token with got code and client_id, client_secret, redirect_url, grant_type - there will be returned Access&Refresh tokens, expiration and token type as JSON